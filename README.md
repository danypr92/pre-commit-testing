# Pre Commit Testing


## Getting started

This repository is for a POC of pre-commit tool.
This tool allows run some checks and validations first to commit the changes.

## Files

* `.python-version` - Version (virtualenv) of Python used for this project.
* `requirements.txt` - Python dependencies needed in this project.
* `.pre-commit-config.yaml` - Configuration of the PreCommit tasks.

## Steps to reproduce in your local

1. Install Pyenv
2. Build Python 3.9.5 with Pyenv.
3. Create a virtualenv called `precommit`.
4. Clone this repository.
5. Access to the repository folder and create a new commit.
6. The output of precommit is showed.
